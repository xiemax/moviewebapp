import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class MovieSearchService {
    constructor(private httpClient: HttpClient) { }

    searchMovie(data: any) {
        let params = new HttpParams()
        params = params.append('s', data.data1);
        params = params.append('y', data.data2);
        return this.httpClient.get<any>('http://www.omdbapi.com/?apikey=af882e17&type=movie', { params: params });
    }

    movieDetail(id:any){
        return this.httpClient.get<any>('http://www.omdbapi.com/?apikey=af882e17&plot=full&i=' + id );
    }

    searchMovieList(data: any) {
        let params = new HttpParams();
        params = params.append('s', data.data1);
        params = params.append('y', data.data2);
        return this.httpClient.get<any>('http://www.omdbapi.com/?apikey=af882e17&type=movie', { params: params });
    }
}