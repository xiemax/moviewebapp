import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MovieSearchService } from './app.services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'home-page',
    templateUrl: 'home-page.component.html',
    styleUrls: ['./app.component.css'],
    providers: [MovieSearchService]
})

export class HomeComponent implements OnInit {

    titleHeader = 'Movie Information Web App';

    @Input() searchMovieList = { name: '', years: '' }

    id: any;
    movieDetail: any;

    chosenYearDate: Date;
    repportSearch: boolean = false;

    searchListMovie: any = [];
    dataTitle: string;
    dataYears: string;

    movieForm = this.formBuilder.group({
        search: ['', Validators.required],
        years: ['', Validators.required],
    });

    constructor(
        private formBuilder: FormBuilder,
        private movieService: MovieSearchService,
        private route: ActivatedRoute
    ) {
    }

    getMovie($event: any) {
        this.movieService.searchMovie({ data1: this.movieForm.get('search').value, data2: this.movieForm.get('years').value })
            .subscribe(
                (result: any) => {
                    this.repportSearch = false;
                    this.searchListMovie = result.Search;
                    this.id = this.searchListMovie.imdbID;
                }
            )
    }

    getDetail(id) {
        this.movieService.movieDetail(this.id)
            .subscribe(
                result => {
                    // tslint:disable-next-line: quotemark
                    console.log("DETAIL INFO : ", result);
                    this.movieDetail = result.Search;
                }
            )
    }

    ngOnInit(){

    }
}