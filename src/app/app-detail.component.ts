import { Component, OnInit } from '@angular/core';
import { MovieSearchService } from './app.services';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
    selector: 'page-detail',
    templateUrl: 'app-detail.component.html',
    styleUrls: ['./app.component.css'],
})

export class DetailComponent implements OnInit {

    id: number;
    detail:any;

    titleHeader = 'Movie Information Web App';

    constructor(
        private movieService: MovieSearchService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.getDetail();
    }

    private getDetail(){
        this.id = this.route.snapshot.params.id;
        console.log(this.id);
        this.movieService.movieDetail(this.id)
                        .subscribe(
                            (result: any) => {
                                console.log("DETAIL INFO : ", result);
                                this.detail = result;
                            }
        )
    }
}